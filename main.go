package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type Country struct {
	Confirmed     int64   `json:"confirmed"`
	Recovered     int64   `json:"recovered"`
	Deaths        int64   `json:"deaths"`
	Country       string  `json:"country"`
	MortalityRate float64 `json:"mortalityRate"`
	Updated       string  `json:"updated"`
}

type InformationCountry struct {
	All Country `json:"All"`
}

type CountryVaccine struct {
	Administered              int64  `json:"administered"`
	PeopleVaccinated          int64  `json:"people_vaccinated"`
	PeoplePartiallyVaccinated int64  `json:"people_partially_vaccinated"`
	Country                   string `json:"country"`
}

type InformationCountryVaccine struct {
	All CountryVaccine `json:"All"`
}

type Information map[string]*InformationCountry

type InformationVaccine map[string]*InformationCountryVaccine

func getAllInformation(c echo.Context) error {
	resp, err := http.Get("https://covid-api.mmediagroup.fr/v1/cases")
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var res Information
	json.Unmarshal(body, &res)
	for k, _ := range res {
		if res[k].All.Confirmed != 0 {
			res[k].All.MortalityRate = float64(res[k].All.Deaths) / float64(res[k].All.Confirmed)
		}
	}
	return c.JSON(http.StatusCreated, res)
}

func getLastInformation(c echo.Context) error {
	var lastday int
	p := c.QueryParam("lastday")
	if p == "" {
		lastday = 7
	} else {
		i, err := strconv.Atoi(p)
		if err != nil {
			return err
		}
		lastday = i

	}
	resp, err := http.Get("https://covid-api.mmediagroup.fr/v1/cases")
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var res Information
	json.Unmarshal(body, &res)
	layoutISO := "2006/01/02"
	for k, _ := range res {
		a := res[k].All.Updated
		if a != "" {
			fmt.Println(a)
			t, err := time.Parse(layoutISO, a[:10])

			if err != nil {
				return err
			}
			date := time.Now()
			diff := date.Sub(t)
			datediff := int(diff.Hours() / 24)
			if datediff < lastday {
				if res[k].All.Confirmed != 0 {
					res[k].All.MortalityRate = float64(res[k].All.Deaths) / float64(res[k].All.Confirmed)
				}
			}
		} else {
			delete(res, k)
		}

	}
	return c.JSON(http.StatusCreated, res)
}
func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func getInformationByCountryNames(c echo.Context) error {
	q := c.QueryParams()
	values := q["countries"]
	resp, err := http.Get("https://covid-api.mmediagroup.fr/v1/cases")
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var res Information
	json.Unmarshal(body, &res)
	for k, _ := range res {
		if res[k].All.Confirmed != 0 {
			res[k].All.MortalityRate = float64(res[k].All.Deaths) / float64(res[k].All.Confirmed)
		}
		if !contains(values, k) {
			delete(res, k)
		}
	}
	return c.JSON(http.StatusCreated, res)
}

func getInformationOfVaccine(c echo.Context) error {
	q := c.QueryParams()
	values := q["countries"]
	resp, err := http.Get("https://covid-api.mmediagroup.fr/v1/vaccines")
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var res InformationVaccine
	json.Unmarshal(body, &res)
	for k, _ := range res {
		if !contains(values, k) {
			delete(res, k)
		}
	}
	return c.JSON(http.StatusCreated, res)
}

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/api/v1/information-all-around-the-world", getAllInformation)
	e.GET("/api/v1/information-last-days-around-the-world", getLastInformation)

	// can filter multiple conutries
	e.GET("/api/v1/information-by-country-names", getInformationByCountryNames)

	e.GET("/api/v1/information-of-vaccines-by-country-names", getInformationOfVaccine)

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}
